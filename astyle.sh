#!/bin/bash

CMD=`type -p astyle`

if [ ! -x "$CMD" ]; then
   # not found exit
   echo "please install astyle and restart this script"
   exit 1
fi

ASTYLE2_VERSION="Artistic Style Version 2."
ASTYLE3_VERSION="Artistic Style Version 3."
 
ARTISTIC_STYLE_OPTIONS=" \
--mode=c \
--style=k&r \
--indent=spaces=4 \
--indent-classes \
--indent-switches \
--indent-col1-comments \
--indent-preprocessor \
--break-blocks \
--pad-oper \
--add-brackets \
--convert-tabs \
--formatted \
--lineend=linux "

if [[ "`$CMD --version 2>&1`" == ${ASTYLE3_VERSION}* ]]; then

ARTISTIC_STYLE_OPTIONS="$ARTISTIC_STYLE_OPTIONS --pad-comma --add-braces"

elif [[ "`$CMD --version 2>&1`" == ${ASTYLE2_VERSION}* ]]; then

ARTISTIC_STYLE_OPTIONS="$ARTISTIC_STYLE_OPTIONS --add-brackets"

else
	echo "Requested version 2 or 3"
	exit 2
fi

set -e

$CMD $ARTISTIC_STYLE_OPTIONS --suffix=none --recursive  "sources/*.cpp" "sources/*.h"
