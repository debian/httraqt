SET(HTTRAQT_HTTRACK_FOUND 0)

# INCLUDE(HTTRAQTFindPkgConfig)
# PKG_CHECK_MODULES(HTTRACK libhttrack2)


SET ( HT_DIR "/usr/include/httrack/" )
IF(EXISTS "${HT_DIR}" AND IS_DIRECTORY "${HT_DIR}")
    SET ( HTTRACK_INCLUDES_DIR "${HT_DIR}" )
    INCLUDE_DIRECTORIES(${HTTRACK_INCLUDES_DIR})
ELSE()
    SET ( HT_DIR "/usr/local/include/httrack/" )
    IF(EXISTS "${HT_DIR}" AND IS_DIRECTORY "${HT_DIR}")
        SET ( HTTRACK_INCLUDES_DIR "${HT_DIR}" )
        INCLUDE_DIRECTORIES(${HTTRACK_INCLUDES_DIR})
    ENDIF()
ENDIF()


IF( NOT HTTRACK_INCLUDES_DIR )
    MESSAGE(FATAL_ERROR "Please INSTALL the httrack, httrack-dev packages and try again")
#         RETURN()
ELSE()
    SET (HTTRACK_FOUND 1)
ENDIF()

MESSAGE("httrack header directory found: " ${HTTRACK_INCLUDES_DIR})

FIND_LIBRARY(
    HTTRACK_LIBRARY NAMES httrack libhttrack.lib
    PATHS /usr/local/lib /usr/lib
    HINTS ${httrack_dirs1} ${httrack_dirs2} ${STAGING_LIBS_DIR}
    DOC "Path to httrack library."
)

IF(NOT HTTRACK_LIBRARY)
    MESSAGE(
        FATAL_ERROR
        "Could not find httrack library.\n"
        "You may need to INSTALL a package named libhttrack-dev or similarly."
    )
ENDIF()

IF(HTTRACK_FOUND)
        SET(HTTRAQT_HTTRACK_INCLUDE_DIRS
                ${HTTRACK_INCLUDES_DIR}
                )

        SET(HTTRAQT_HTTRACK_LIB_DIRS
                ${HTTRACK_LIBRARY_DIRS}
                )

        SET(HTTRAQT_HTTRACK_LIBS
                ${HTTRACK_LIBRARIES}
                )

        SET(HTTRAQT_HTTRACK_FOUND 1)
ENDIF(HTTRACK_FOUND)
