httraqt (1.4.11-1) unstable; urgency=medium

  * [0616b86] New upstream version 1.4.11. (Closes: #1020677)
  * [9bd1e36] Refresh patches.
  * [afc5a94] Update watch file format version to 4.
  * [b48f362] Use secure URI in Homepage field.
  * [bb713de] Update standards version to 4.6.1, no changes needed.
  * [c0b3ad1] Switch to QT6.
  * [3314f70] Drop doc-base file
  * [999eeed] Override false positive lintian error

 -- Anton Gladky <gladk@debian.org>  Tue, 31 Jan 2023 07:50:23 +0100

httraqt (1.4.9-5) unstable; urgency=medium

  * Install doc-files in /usr/share/httraqt. (Closes: #990895)

 -- Anton Gladky <gladk@debian.org>  Sat, 10 Jul 2021 22:16:58 +0200

httraqt (1.4.9-4) unstable; urgency=medium

  * [936829d] Fix section in manpage. (Closes: #963343)
  * [bfcbef7] Set upstream metadata fields: Repository.

 -- Anton Gladky <gladk@debian.org>  Sun, 28 Jun 2020 22:51:33 +0200

httraqt (1.4.9-3) unstable; urgency=medium

  * [73a80e9] Fix FTCBFS. (Closes: #917999)

 -- Anton Gladky <gladk@debian.org>  Tue, 05 May 2020 22:03:16 +0200

httraqt (1.4.9-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Anton Gladky ]
  * [ec12447] Trim trailing whitespace.
  * [1211c30] Bump debhelper from old 10 to 13.
  * [2b59c22] Set debhelper-compat version in Build-Depends.
  * [7bc16bb] Set Standards-Version to 4.5.0
  * [2058dca] Add Rules-Requires-Root: no
  * [5a1c40e] Add debian/.gitlab-ci.yml
  * [fb206c6] Move docs into the /usr/share/doc/htttraqt

 -- Anton Gladky <gladk@debian.org>  Mon, 04 May 2020 07:17:24 +0200

httraqt (1.4.9-1) unstable; urgency=medium

  * [fa67d0e] Update d/watch.
  * [c05647b] Set maximal hardening level.
  * [6eff4ab] Update patches
  * [167a718] New upstream version 1.4.9

 -- Anton Gladky <gladk@debian.org>  Tue, 18 Jul 2017 23:23:36 +0200

httraqt (1.4.8-1) unstable; urgency=medium

  * [4270897] Apply cme fix dpkg.
  * [0ed07ed] Use compat level 10.
  * [8a0fa26] New upstream version 1.4.8

 -- Anton Gladky <gladk@debian.org>  Fri, 20 Jan 2017 20:14:59 +0100

httraqt (1.4.7-1) unstable; urgency=medium

  * [ebd5cc4] Imported Upstream version 1.4.7
  * [7fb1de2] Ignore quilt dir

 -- Anton Gladky <gladk@debian.org>  Sun, 06 Dec 2015 12:39:24 +0100

httraqt (1.4.6-2) unstable; urgency=medium

  * [3c3588e] Take build date from d/changelog. Towards reproducibility.

 -- Anton Gladky <gladk@debian.org>  Mon, 07 Sep 2015 18:39:13 +0200

httraqt (1.4.6-1) unstable; urgency=medium

  * [284a7cf] Imported Upstream version 1.4.6
  * [a52f714] Switch to Qt5.

 -- Anton Gladky <gladk@debian.org>  Sun, 06 Sep 2015 21:22:30 +0200

httraqt (1.4.5-1) unstable; urgency=medium

  * [f7792bd] Imported Upstream version 1.4.5
  * [d5dd257] Standards-Version: 3.9.6. No changes.
  * [8509702] Update d/copyright.
  * [7c6636f] Apply cme fix dpkg-control.

 -- Anton Gladky <gladk@debian.org>  Sat, 05 Sep 2015 22:04:12 +0200

httraqt (1.4.4-1) unstable; urgency=medium

  * [b710f90] Imported Upstream version 1.4.4

 -- Anton Gladky <gladk@debian.org>  Fri, 25 Jul 2014 23:25:15 +0200

httraqt (1.4.3-1) unstable; urgency=medium

  * [853a145] Imported Upstream version 1.4.3

 -- Anton Gladky <gladk@debian.org>  Tue, 10 Jun 2014 23:35:47 +0200

httraqt (1.3.2-1) unstable; urgency=medium

  * [d7782ee] Imported Upstream version 1.3.2

 -- Anton Gladky <gladk@debian.org>  Mon, 28 Apr 2014 19:30:15 +0200

httraqt (1.3.1-1) unstable; urgency=medium

  * [be7896b] Imported Upstream version 1.3.1

 -- Anton Gladky <gladk@debian.org>  Sun, 23 Mar 2014 07:18:08 +0100

httraqt (1.3.0-1) unstable; urgency=medium

  * [21d0560] Imported Upstream version 1.3.0

 -- Anton Gladky <gladk@debian.org>  Fri, 31 Jan 2014 19:57:07 +0100

httraqt (1.2.2-1) unstable; urgency=medium

  * [df79bef] Imported Upstream version 1.2.2

 -- Anton Gladky <gladk@debian.org>  Thu, 23 Jan 2014 19:47:53 +0100

httraqt (1.2.1-1) unstable; urgency=medium

  * [687f3bb] Imported Upstream version 1.2.1

 -- Anton Gladky <gladk@debian.org>  Sat, 04 Jan 2014 23:03:48 +0100

httraqt (1.2.0-1) unstable; urgency=low

  * Initial packaging. (Closes: #733448).

 -- Anton Gladky <gladk@debian.org>  Sun, 29 Dec 2013 22:18:46 +0100
