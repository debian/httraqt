/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QtGui>
#include <QUrl>

#include "includes/httraqt.h"
#include "includes/AboutDialog.h"
#include "../version.h"


AboutDialog::AboutDialog(QWidget *parent, Qt::WindowFlags fl)
    : QDialog(parent, fl)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    setStyleSheet(this->parent->programStyleSheet);

    translateDialog();
    connect(pushButton, SIGNAL(clicked()), this, SLOT(reject()));
    label_2->setPixmap(*(this->parent->mainpix));

    adjustSize();
}


void AboutDialog::translateDialog()
{
    if (parent->programStyleSheet.length() > 0) {
        setStyleSheet(parent->programStyleSheet);
    }

    pushButton->setText(translate(_CLOSE));

    QString str2 = translate(_NOTIFY);
    str2.replace("\n", "<br>");

    QString str4 = translate(_VISITPAGE);
#if USE_QT_VERSION == 6
    QString outStr = QString().asprintf(PROGRAM_FULL_NAME, HTTQTVERSION) + "<br>" + str2 + "<br><br>" + str4;
#else
    QString outStr = QString().sprintf(PROGRAM_FULL_NAME, HTTQTVERSION) + "<br>" + str2 + "<br><br>" + str4;
#endif
    outStr += "<br>HTTrack <a href=\"" + QString(HTTRACK_STR) + "\">" + QString(HTTRACK_STR) + "</a>";
    outStr += "<br>HTTraQt <a href=\"" + QString(HTTRAQT_STR) + "\">" + QString(HTTRAQT_STR) + "</a>";

    label->setText(outStr);

    QString t = "<b>" + translate(_HOW_THANK) + "</b><br><br>"
                "- " + translate(_SHARE_LINK) + "<br>"
                "- " + translate(_REPORT_BUG) + "<br>"
                "- " + translate(_MAKE_DONATE) + ". " + translate(_MAKE_TRANSLATION);

    label_3->setText(t);
    label_3->setTextFormat(Qt::RichText);
    label_3->setTextInteractionFlags(Qt::TextBrowserInteraction);
    label_3->setOpenExternalLinks(true);
}

