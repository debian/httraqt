/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/httraqt.h"
#include "includes/ConfirmTab.h"

ConfirmTab::ConfirmTab(QWidget *parent, Qt::WindowFlags fl) : QWidget(parent, fl)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    lineEditCounter->setText("5");

    connect(radioSave, SIGNAL(toggled(bool)), this, SLOT(onRadio(bool)));
    connect(radioStart, SIGNAL(toggled(bool)), this, SLOT(onRadio(bool)));

#ifdef __WIN32__
    // code for win systems
    //     qDebug() << "WIN32";
    groupPCoff->setEnabled(false);
#endif


#ifdef __APPLE__
    groupPCoff->setEnabled(false);
#endif


#ifdef __linux__
    //     qDebug() << "LINUX";
    groupPCoff->setEnabled(true);
#endif // LINUX
}


void ConfirmTab::onRadio(bool b)
{
    QRadioButton* bu = (QRadioButton*) sender();

    disconnect(radioSave, SIGNAL(toggled(bool)), this, SLOT(onRadio(bool)));
    disconnect(radioStart, SIGNAL(toggled(bool)), this, SLOT(onRadio(bool)));

    if (bu == radioSave) {
        bool ch = radioSave->isChecked();
        radioStart->setChecked(!ch);
    }

    if (bu == radioStart) {
        bool ch = radioStart->isChecked();
        radioSave->setChecked(!ch);
    }

    connect(radioSave, SIGNAL(toggled(bool)), this, SLOT(onRadio(bool)));
    connect(radioStart, SIGNAL(toggled(bool)), this, SLOT(onRadio(bool)));
}

void ConfirmTab::resizeEvent(QResizeEvent *event)
{
}

void ConfirmTab::translateTab()
{
    setMinimumSize(410, 250);

    if (parent->programStyleSheet.length() > 0) {
        setStyleSheet(parent->programStyleSheet);
    }

    radioStart->setText(translate(_ADJ));
    radioShutdown->setText(translate(_SHUTD));
    radioHibernate->setText(translate(_HIBER));
    label05->setText(translate(_TR_SHED));
    radioSave->setText(translate(_SAV_SETTINGS));
    group02->setTitle(translate(_ON_HOLD));
    groupPCoff->setTitle(translate(_PC_OFF));
    labelCounter->setText(translate(_SHUTDOWN_COUNTER));
}



