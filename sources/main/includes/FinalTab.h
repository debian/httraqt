/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef FINWIDGET_H
#define FINWIDGET_H

#include <QWidget>

#include "httraqt.h"
#include "ui_FinalTab.h"


class HTTraQt;

class FinalTab: public QWidget, public Ui::FinalTab, public cTranslator
{
        Q_OBJECT
    public:
        FinalTab(QWidget *parent = 0, Qt::WindowFlags fl = Qt::WindowType::Widget);
    public slots:
        void translateTab(void);

    protected:
        void resizeEvent(QResizeEvent *event);

    private slots:
        void onBrowseLocalWebsite();
        void onViewLog();

    private:
        HTTraQt* parent;
};

#endif
