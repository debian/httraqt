/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef MAINOPTIONS_H
#define MAINOPTIONS_H

#include <QString>
#include <QWidget>

#include "httraqt.h"
#include "ui_OptionsTab.h"


class HTTraQt;

class OptionsTab: public QWidget, public Ui::OptionsTab, public cTranslator
{
        Q_OBJECT
    public:
        OptionsTab(QWidget *parent = 0, Qt::WindowFlags fl = Qt::WindowType::Widget);
        bool testIt();
        void translateTab();
        void updateLastAction();

    protected:
        void resizeEvent(QResizeEvent *event);

    private slots:
        void onSetOptions();
        void onURLList();
        void showHelp();
        void removeSpaceLines();
        void onCaptureUrl();
        void onAddURL();
        QString getAndTestUrlString(const QString st);

    public:
        int comb01;
        static int hl01[7];

    private:
        HTTraQt* parent;
};

#endif
