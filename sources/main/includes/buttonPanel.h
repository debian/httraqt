/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef BUTTONPANEL_H
#define BUTTONPANEL_H

#include "httraqt.h"
#include "ui_buttonPanel.h"


class HTTraQt;

class buttonPanel: public QWidget, public Ui::buttonsForm, public cTranslator
{
        Q_OBJECT
    public:
        buttonPanel(QWidget *parent = 0, Qt::WindowFlags fl = Qt::WindowType::Widget);
        void translateButtons();
        void onButtons(int n);

    public slots:
        void onHelp();
        void onStop();
        void onNext();
        void onBack();

    private:
        HTTraQt* parent;

};

#endif
