/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

using namespace std;

#include "../../version.h"

#include <QMainWindow>
#include <QWidget>
#include <QCloseEvent>
#include <QStringList>
#include <QString>

#if USE_QT_VERSION == 6
#include <QFileSystemModel>
#else
#include <QDirModel>
#endif


#include <QPixmap>
#include <QMessageBox>
#include <QSystemTrayIcon>
#include <QTimer>
#include <QtGui>
#include <QGroupBox>
#include <QtCore/QSharedMemory>


#ifndef HTTRAQ_H
#define HTTRAQ_H


// #include "../../icons/main.h"

#include "translator.h"
#include "options.h"

#include "ui_mainForm.h"


#define PROGRAM_NAME        "HTTraQt"
#define PROGRAM_FULL_NAME   "HTTraQt Website Copier v.%s (Qt4/Qt5 based)"

#define LAST_ACTION         6

/* Location */
#define WHTT_LOCATION(a) WhttLocation=(a)

#ifndef HTS_DEF_FWSTRUCT_lien_back
#define HTS_DEF_FWSTRUCT_lien_back

#endif

#ifdef __cplusplus
extern "C"
{
#endif

// #include "includes/htinterface.h"


#include <httrack-library.h>
#include <htsglobal.h>
#include <htswrap.h>
#include <htsopt.h>
#include <htsdefines.h>


#ifdef __cplusplus
}
#endif


#define NStatsBuffer     14
#define MAX_LEN_INPROGRESS 40

#include "htinterface.h"


class buttonPanel;
class QAction;
class QMenu;
class QTextEdit;


struct hInfo {
    QString name;
    QString tInfo;
    int num;
};



typedef struct {
    QString name;
    QString file;
    QString strStatus;
    QString url_sav;    // pour cancel
    QString url_adr;
    QString url_fil;
    qint64 size;
    qint64 sizeTotal;
    int offset;
    int back;
    int actived;    // pour disabled
} t_StatsBuffer;


class MessageTimerBox: public QMessageBox
{
    private:
        int timeout;
        bool autoClose;
        int currentTime;
        QString defaultText;

    public:
        void setTimeout(int t);
        void setDefaultText(const QString &t);
        void setAutoClose(bool a);
        void showEvent ( QShowEvent * event );

    private:
        void timerEvent(QTimerEvent *event);
        //     static int exec(const QString &title, const QString &text, int ticon, int timer);
};


class MessageBox: public cTranslator
{
    public:
        static int exec(void* p, const QString &title, const QString &text, int ticon);
};


#if USE_QT_VERSION == 6
class DirModel : public QFileSystemModel
{
    public:
        QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
};
#else
class DirModel : public QDirModel
{
    public:
        QVariant headerData ( int section, Qt::Orientation orientation, int role ) const;
};
#endif

class MyThread : public QThread
{
    public:
        void run();
};


class HTTraQt: public QMainWindow, public Ui::MainWindow, public cTranslator, public cOptions
{
        Q_OBJECT

    public:
        HTTraQt(QWidget* parent = 0, Qt::WindowFlags flags = Qt::WindowType::Widget);
        ~HTTraQt();
        bool rebuildWorkDirView();
        void activatePage(int pagenum);
        void onCancelAll();
        void onQuit();
        bool getLangTable();
        void setLangGUI();
        void translateActions();
        void translateActions(QAction* act, int id);
        void launch();
        void getMainOptionsFromGUI();
        void setMainOptionsToGUI();
        void initSInfo();

        void readGUISettings();
        void readSettings(bool global);
        void writeSettings(bool global);
        void writeGUISettings(void);

        void clearStatsBuffer(void);
        void setFontForWidgets(void);
        void getOptions();
        void onEndMirror();
        bool checkContinue(bool msg);
        void afterChangepathlog();
        //         void resetDefaultOptions();
        bool RemoveEmptyDir(QString path);

    public slots:
        void pauseTransfer();
        void onStopAll();
        void displayOutputMsg();
        void displayProgressMsg();
        void processFinished(int exitCode, QProcess::ExitStatus exitStatus);
        bool treeItemClicked(const QModelIndex &m);
        //         void resizeTabs(QResizeEvent* re);
        void refreshDirModel();

    protected:
        void closeEvent(QCloseEvent *event);
        //         void changeEvent(QEvent* re);

    private slots:
        void threadFinished();
        void newProject();
        void browseSites();
        void setLang(QAction* mnu);
        void quit();
        void loadDefaultOptions();
        void saveDefaultOptions();
        void resetToDefault();
        void loadOptionsFromFile();
        void saveOptionsAs();
        void selectFontSize(QAction* mnu);
        void modifyOptions();
        void setOptions();
        void viewLog();
        void viewErrorLog();
        void viewTransfers();
        void hideWindow();
        void restoreWindow();
        //         void createFontSizeMenu();
        bool readLangDir();
        void checkUpdates();
        void contens();
        void stepByStep();
        void about();
        void aboutQt();
        void iconActivated(QSystemTrayIcon::ActivationReason reason);

    private:
        void shutdown();
        void hibernate();
        void createTrayIcon();
        void renameOldToNew();
        void convertTranslateFiles();
        QString sizeToText(qint64 s);
        void gOptions(bool d);
        QString getLocaleString();
        int  removeFolder(QDir &dir);
        bool getStrOption(const QString optName, QString &ret);
        bool getIntOption(const QString optName, int &ret);
        void createActions();
        void createToolBars();
        void createStatusBar();

        //         void cmdArgumentsOptions();

        QString change(char* chaine, char c);
        bool checkInstanceRuns();

        void contextMenuEvent(QContextMenuEvent *);

    public:
        QPixmap *mainpix;
        QPixmap *mainicon;
        QTimer *timerProgressUpdate;
        QTimer *timerDirRefresh;
        QWidget *widgets[6];
        //         QVector<options> prOptions;
        //         QMap<QString, OptStruct> prOptions;
        buttonPanel* buttonsWidget;

        int  m_todo;

    private:
        QSharedMemory *_singular;
        QString filename;

        MyThread* mth;
        QString agumentsLine;
        QStringList lastDirs;
        QVector<QAction*> actLangSelect;
        QVector<QAction*> actFSizeSelect;
        QMenu *langMenu;
        QAction* restAct;
        QActionGroup* langGroup;
        //         int dictFormat; // 0 no file, 1 old, 2 new
        // System tray icon
        bool verbose;
        bool pcShutdown;
        bool pcHibernate;
        // System tray menu
        QMenu* trayIconMenu;

    public:
        QStringList lst;
        char* argv[256];
        short argc;
        QProcess* process;
        QString cmdOpt;
        // flag de termination
        int termine;
        static int arretbrutal;
        int termine_requested;
        int shell_terminated;
        bool use_show;
        int soft_term_requested;
        httrackp *global_opt;
        bool inprogress;
        int currentTab;
        QFont sysFont;
        short fontSize;
        QString programStyleSheet;
        QSystemTrayIcon* trayIcon;

        /*static*/
        t_InpInfo SInfo;
        t_StatsBuffer StatsBuffer[NStatsBuffer]; // interface to progres tab


        QStringList langFiles;
        DirModel dModel;

        QMutex mutex;
        QString currentOptionsFile;
        QString defaultOptionsFile;
        QString helpDir;
        QString langDir;
        QString currentLang;
};

#endif
