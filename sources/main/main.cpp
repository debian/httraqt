/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QApplication>
#include <QtGui>
#include "includes/httraqt.h"
#include "../version.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
#if 0
    QPixmap logo(":/images/splash.png");
    QPainter painter(&logo);
    painter.setFont(QFont("Arial", 15));

    QSplashScreen* splash = new QSplashScreen(logo);
    splash->show();
    splash->showMessage(QObject::tr(PROGRAM_FULL_NAME), Qt::AlignLeft | Qt::AlignBottom);
#endif


#if QT_VERSION < 0x050000
    // ask QString in Qt 4 to interpret all char* as UTF-8,
    // like Qt 5 does.
    // 106 is "UTF-8", this is faster than lookup by name
    // [http://www.iana.org/assignments/character-sets/character-sets.xml]
    QTextCodec::setCodecForCStrings(QTextCodec::codecForMib(106));
    // and for reasons I can't understand, I need to do the same again for tr
    // even though that clearly uses C strings as well...
    QTextCodec::setCodecForTr(QTextCodec::codecForMib(106));
#ifdef Q_OS_WIN
    QFile::setDecodingFunction(decodeUtf8);
    QFile::setEncodingFunction(encodeUtf8);
#endif
#else
    // for Win32 and Qt5 we try to set the locale codec to UTF-8.
    // this makes QFile::encodeName() work.
#ifdef Q_OS_WIN
    QTextCodec::setCodecForLocale(QTextCodec::codecForMib(106));
#endif
#endif


    HTTraQt* mainWidget;
    QString pName = QString(PROGRAM_NAME) + " v." + QString(HTTQTVERSION);
    mainWidget = new HTTraQt(0, Qt::Window);
    mainWidget->setLangGUI();
    mainWidget->setWindowTitle(pName);

    mainWidget->show();

    return app.exec();
}

