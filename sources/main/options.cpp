/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QDir>
#include <QDebug>

#include <htsstrings.h>
#include "includes/options.h"
#include "../version.h"

// init of static object
QMap<QString, QVariant> cOptions::prOptions;


void cOptions::SetProfile(const QString &variName, float val)
{
    if (variName.length() < 1) {
        return;
    }

    QVariant tmp = prOptions[variName];

    if (tmp.type() != QMetaType::Float) {
        qDebug() << "Wrong type, setProfile()" << variName << "was expected FLOAT!";
        return;
    }

    //     tmp = val;
    prOptions[variName] = val;
}



void cOptions::SetProfile(const QString &variName, const QVariant &val)
{
    if (variName.length() < 1) {
        return;
    }

    //     tmp = val;
    prOptions[variName] = val;
}


void cOptions::SetProfile(const QString &variName, int val)
{
    if (variName.length() < 1) {
        return;
    }

    QVariant tmp = prOptions[variName];

    if (tmp.type() != QMetaType::Int) {
        qDebug() << "Wrong type, setProfile()" << variName << "was expected INTEGER!";
        return;
    }

    //     tmp.value = val;
    prOptions[variName] = val;
}


void  cOptions::SetProfile(const QString &variName, const QString &val)
{
    if (variName.length() < 1) {
        return;
    }

    QVariant tmp = prOptions[variName];

    if (tmp.type() != QMetaType::QString) {
        qDebug() << "Wrong type, setProfile()" << variName << "was expected TEXT!";
        return;
    }

    //     tmp.value = val;
    prOptions[variName] = val;
}


QVariant cOptions::GetProfile(const QString &variName)
{
    if (variName.length() < 1) {
        return -1;
    }

    return prOptions[variName];
}


void cOptions::GetProfile(const QString &variName, float &val)
{
    if (variName.length() < 1) {
        return;
    }

    QVariant tmp = prOptions[variName];

    if (tmp.type() != QMetaType::Float) {
        qDebug() << "Wrong type, getProfile()" << variName << "was expected FLOAT!";
        return;
    }

    val = tmp.toFloat();
}


void cOptions::GetProfile(const QString &variName, int &val)
{
    if (variName.length() < 1) {
        return;
    }

    QVariant tmp = prOptions[variName];

    if (tmp.type() != QMetaType::Int) {
        qDebug() << "Wrong type, getProfile()" << variName << "was expected INTEGER!";
        return;
    }

    val = tmp.toInt();
}


void cOptions::GetProfile(const QString &variName, QString &val)
{
    if (variName.length() < 1) {
        return;
    }

    QVariant tmp = prOptions[variName];

    if (tmp.type() != QMetaType::QString) {
        qDebug() << "Wrong type, getProfile()" << variName << "was expected TEXT!";
        return;
    }

    val = tmp.toString();
}


void cOptions::saveOptions(QSettings *s, bool gl)
{
    QString os, st, br;
    GetProfile("BrowserID", st);
    GetProfile("BrowserName", br);
    GetProfile("osID", os);

    if (st.length() > 0) {
        if (st.contains("%s")) {
            st.replace("%s", os);
        }

        SetProfile("UserID", st);
    }

    for (QMap<QString, QVariant>::const_iterator iopt = prOptions.constBegin(); iopt != prOptions.constEnd(); ++iopt) {
        if (gl == true) { // do not save as global
            if (iopt.key() == "CurrentUrl" || iopt.key() == "CurrentURLList") { // only in project settings
                continue;
            }
        }

        if ((*iopt).type() == QMetaType::QString) {
            QString v = (*iopt).toString();
            profile_code(v);

            s->setValue(iopt.key(), v);
        }

        if ((*iopt).type() == QMetaType::Float) {
            if ((*iopt).toFloat() >= 0) {
                float fTmp = (*iopt).toFloat();
                QString fTxt;
#if USE_QT_VERSION == 6
                fTxt = QString().asprintf("%6.4f", fTmp);
#else
                fTxt = QString().sprintf("%6.4f", fTmp);
#endif
                //qDebug() << "save" << (*iopt).name << fTxt;
                s->setValue(iopt.key(), (qint64)( fTxt.toFloat() * 1024.0 * 1024.0));
            } else {
                s->setValue(iopt.key(), "");
            }
        }

        if ((*iopt).type() == QMetaType::Int) {
            if ((*iopt).toInt() != -1) {
                s->setValue(iopt.key(), (*iopt).toInt());
            } else {
                s->setValue(iopt.key(), "");
            }
        }
    }

    s->sync();
}



// modif RX 10/10/98 pour gestion des , et des tabs
void cOptions::StripControls(QString &st)
{
    //     st.replace(0x09, " ");
    //     st.replace(0x0a, " ");
    //     st.replace(0x0d, " ");
    st.simplified();
    //     st.replace("  ", " ");
}



// Ecriture profiles
void cOptions::profile_code(QString &from)
{
    from.replace("%%", "%");    // delete double entries
    from.replace("\n\n", "\n");    // delete double entries
    from.replace("%", "%%");
    from.replace("\r", "%0a");
    from.replace("\n", "%0d%0a");
    from.replace("\t", "%09");
}


void cOptions::profile_decode(QString &from)
{
    from.replace("%0d%0a", "\n");
    from.replace("%0d", "\n");
    from.replace("%0a", "\r");
    from.replace("%09", "\t");
    from.replace("%%", "%");
}



void cOptions::loadOptions(QSettings *s)
{
    initOptions();
    //     qDebug() << "load";

    for (QMap<QString, QVariant>::iterator iopt = prOptions.begin(); iopt != prOptions.end(); ++iopt) {
        if ((*iopt).type() == QMetaType::QString) {
            QString tStr;
            tStr = s->value(iopt.key(), (*iopt).toString()).toString();
            profile_decode(tStr);
            (*iopt) = tStr;

            continue;
        }

        if ((*iopt).type() == QMetaType::QString) {
            float tFloat;
            bool iOk;
            tFloat = s->value(iopt.key(), (*iopt).toFloat()).toFloat(&iOk);

            if (iOk == true) {
                if (tFloat >= 0) {
                    QString tTxt;
#if USE_QT_VERSION == 6
                    tTxt = QString().asprintf("%6.4f", tFloat / (1024.0 * 1024.0));
#else
                    tTxt = QString().sprintf("%6.4f", tFloat / (1024.0 * 1024.0));
#endif
                    tFloat = tTxt.toFloat();
                }

                //qDebug() << "load" << (*iopt).name << tFloat << (qint64)tFloat;
                (*iopt) = tFloat; // overwrite the default value if right
            } else {
                (*iopt) = -1.0;
            }

            continue;
        }

        if ((*iopt).type() == QMetaType::Int) {
            int tInt;
            bool iOk;
            tInt = s->value(iopt.key(), (*iopt).toInt()).toInt(&iOk);

            if (iOk == true) {
                (*iopt) = tInt; // overwrite the default value if right
            }

            continue;
        }
    }

    // to implement the checking of options file!
}


void cOptions::getOptStruct(httrackp *g_opt)
{
    httrackp *opt = hts_create_opt();

    opt->log = opt->errlog = NULL;

    // dévalider champs (non modifiés)
    opt->maxsite = -1;
    opt->maxfile_nonhtml = -1;
    opt->maxfile_html = -1;
    opt->maxsoc = -1;
    opt->nearlink = -1;
    opt->timeout = -1;
    opt->rateout = -1;
    opt->maxtime = -1;
    //     opt->check_type = 0;
    //     opt->mms_maxtime = -1;
    opt->maxrate = -1;
    StringClear(opt->user_agent);
    opt->retry = -1;
    opt->hostcontrol = -1;
    opt->errpage = -1;
    opt->travel = -1;
    opt->external = -1;
    opt->delete_old = -1;
    opt->parseall = -1;
    opt->delete_old = -1;
    opt->travel = 0;       // NOTE: NE SERA PRIS EN COMPTE QUE LE BIT 8

    int n;
    float nf;
    GetProfile("Test", n);

    if (n) {
        opt->travel |= 256;
    }

    //     GetProfile("CheckType", n);
    //     opt->check_type = n;

    GetProfile("ParseAll", n);

    if (n) {
        opt->parseall = 1;
    } else {
        opt->parseall = 0;
    }

    // near link,err page
    GetProfile("Near", n);

    if (n) {
        opt->nearlink = 1;
    } else {
        opt->nearlink = 0;
    }

    GetProfile("NoErrorPages", n);

    if (n) {
        opt->errpage = 1;
    } else {
        opt->errpage = 0;
    }

    GetProfile("NoExternalPages", n);

    if (n) {
        opt->external = 1;
    } else {
        opt->external = 0;
    }

    GetProfile("NoPurgeOldFiles", n);

    if (n) {
        opt->delete_old = 1;
    } else {
        opt->delete_old = 0;
    }


    // host control
    {
        int a = 0;
        GetProfile("RemoveTimeout", n);

        if (n > 0) {
            a += 1;
        }

        GetProfile("RemoveRateout", n);

        if (n > 0) {
            a += 2;
        }

        opt->hostcontrol = a;
    }

    // sockets
    GetProfile("Sockets", n);

    if (n >= 0) {
        opt->maxsoc = n;
        maxProgressRows = n;
    } else {
        maxProgressRows = 8;
        opt->maxsoc = 8;
    }

    // maxfile_nonhtml
    GetProfile("MaxOther", nf);

    if (nf > 0) {
        opt->maxfile_nonhtml = (qint64)(nf * 1024.0 * 1024.0);
    } else {
        opt->maxfile_nonhtml = -1;
    }

    // maxfile_html
    GetProfile("MaxHtml", nf);

    if (nf > 0) {
        opt->maxfile_html = (qint64)nf * 1024.0 * 1024.0;
    } else {
        opt->maxfile_html = -1;
    }

    // maxsite
    GetProfile("MaxAll", nf);

    if (nf > 0) {
        opt->maxsite = (qint64)(nf * 1024.0 * 1024.0);
    } else {
        opt->maxsite = -1;
    }

    // fragment
    GetProfile("MaxWait", n);

    if (n > 0) {
        opt->fragment = (qint64)n;
    } else {
        opt->fragment = -1;
    }

    // timeout
    GetProfile("TimeOut", n);

    if (n >= 0) {
        opt->timeout = n;
    } else {
        opt->timeout = -1;
    }

    // rateout
    GetProfile("RateOut", n);

    if (n != 0) {
        opt->rateout = n;
    } else {
        opt->rateout = -1;
    }

    // maxtime
    GetProfile("MaxTime", n);

    if (n > 0) {
        opt->maxtime = n;
    } else {
        opt->maxtime = -1;
    }

    // maxrate
    GetProfile("MaxRate", nf);

    if (nf > 0) {
        opt->maxrate = (int)(nf * 1024.0 * 1024.0);
    } else {
        opt->maxrate = -1;
    }

    // max. connect
    GetProfile("MaxConn", n);

    if (n > 0) {
        opt->maxconn = n;
    } else {
        opt->maxconn = -1;
    }

    // retry
    GetProfile("Retry", n);

    if (n > 0) {
        opt->retry = n;
    } else {
        opt->retry = -1;
    }

    // user_agent
    QString os, st, br;
    GetProfile("BrowserID", st);
    GetProfile("BrowserName", br);
    GetProfile("osID", os);

    if (st.length() > 0) {
        if (st.contains("%s")) {
            st.replace("%s", os);
        }

        SetProfile("UserID", st);
        StringCopy(opt->user_agent, st.toLatin1().data());
    }

    if (g_opt != NULL) {
        copy_htsopt(opt, g_opt);
    }

    hts_free_opt(opt);
}


QString cOptions::cmdArgumentsOptions(int num)
{
    QString st;
    int n;
    float nf;
    int n1;
    char choixdeb;
    int action;

    QString cmdOpt = "";
    action = num; //GetProfile("CurrentAction", action);

    QString array;
    array = "wWgY!/i";

    choixdeb = array.at(action).toLatin1();

    if (choixdeb != 'W') {
        cmdOpt += "-q ";
    }

    GetProfile("BuildTopIndex", n);

    if (n == 0) {
        cmdOpt += "-%i0 ";
    } else {
        cmdOpt += "-%i ";
    }

    if (choixdeb == '/') {
        cmdOpt += "-i ";
    } else if (choixdeb != '!') {
        cmdOpt += ("-" + QString(choixdeb) + " ");
    }

    QString url, urllist;
    GetProfile("CurrentUrl", url);
    url.replace("\n", " ");
    url.simplified();

    GetProfile("CurrentURLList", urllist);

    if (url.length() == 0 && urllist.length() == 0) {
        // nothong to download
        return "";
    }

    if (url.length() > 0) { // ersetzen!
        cmdOpt += " " + url + " ";//+"\"";
    }

    if (urllist.length() > 0) {
        cmdOpt += (" -%L \"" + urllist + "\" ");
    }

    GetProfile("Depth", n);

    if (n >= 0) {
        cmdOpt += ("-r" + QString::number(n) + " ");
    }

    GetProfile("ExtDepth", n);

    if (n >= 0) {
        cmdOpt += ("-%e" + QString::number(n) + " ");
    }

    if (choixdeb == '/') {
        cmdOpt += "-C1 ";
    } else {
        GetProfile("Cache", n);

        if (n == 0) {
            cmdOpt += "-C0 ";
        } else {
            cmdOpt += "-C2 ";
        }
    }

    GetProfile("NoRecatch", n);

    if (n != 0) {
        cmdOpt += "-%n ";
    }

    GetProfile("Test", n);

    if (n == 1) {
        cmdOpt += "-t ";
    }

    GetProfile("ParseAll", n);

    if (n == 1) {
        cmdOpt += "-%P ";
    } else {
        cmdOpt += "-%P0 ";
    }

    GetProfile("Near", n);

    if (n == 1) {
        cmdOpt += "-n ";
    }

    GetProfile("NoExternalPages", n);

    if (n != 0) {
        cmdOpt += "-x ";
    }

    GetProfile("NoPurgeOldFiles", n);

    if (n != 0) {
        cmdOpt += "-X0 ";
    }

    GetProfile("NoPwdInPages", n);

    if (n != 0) {
        cmdOpt += "-%x ";
    }

    GetProfile("NoQueryStrings", n);

    if (n != 1) { // include-query-string
        cmdOpt += "-%q ";
    }

    GetProfile("FollowRobotsTxt", n);

    if (n >= 0) {
        cmdOpt += ("-s" + QString::number(n) + " ");
    }

    GetProfile("Cookies", n);

    if (n == 0) {
        cmdOpt += "-b0 ";
    }

    GetProfile("CheckType", n);

    if (n > 0) {
        cmdOpt += ("-u" + QString::number(n) + " ");
    }

    GetProfile("ParseJava", n);

    if (n == 0) {
        cmdOpt += "-j0 ";
    }

    GetProfile("StoreAllInCache", n);

    if (n != 0) {
        cmdOpt += "-k ";
    }

    GetProfile("LogType", n);

    if (n == 1) {
        cmdOpt += "-z ";
        GetProfile("Debug", n);

        if (n == 1) {
            cmdOpt += "-%H ";
        }
    } else if (n == 2) {
        cmdOpt += "-Z ";
        GetProfile("Debug", n);

        if (n == 1) {
            cmdOpt += "-%H ";
        }
    }

    GetProfile("HTTP10", n);

    if (n != 0) {
        cmdOpt += "-%h ";
    }

    GetProfile("TolerantRequests", n);

    if (n > 0) {
        cmdOpt += "-%B ";
    }

    GetProfile("UpdateHack", n);

    if (n > 0) {
        cmdOpt += "-%s ";
    }

    GetProfile("URLHack", n);

    if (n > 0) {
        cmdOpt += "-%u ";
    } else {
        cmdOpt += "-%u0 ";
    }

    if (choixdeb != 'g') {
        QString bld;

        GetProfile("Build", n);

        if (n == 14) {   // i.O.
            GetProfile("BuildString", st);
            bld = ("-N \"" + st + "\"");
            //             cmdOpt += bld;
        } else {
            if (n >= 0 && n <= 5) {
                bld = "-N" + QString::number(n);
            } else if (n >= 6 && n <= 11) {
                bld = "-N10" + QString::number(n - 6);
            } else if (n == 12) {
                bld = "-N99";
            } else if (n == 13) {
                bld = "-N199";
            }
        }

        if (bld.length() > 0) {
            cmdOpt += (bld + " ");
        }
    }

    GetProfile("Dos", n);
    GetProfile("ISO9660", n1);

    if (n > 0) {
        cmdOpt += "-L0 ";
    } else if (n1 > 0) {
        cmdOpt += "-L2 ";
    }

    GetProfile("Index", n);

    if (n == 0) {
        cmdOpt += "-I0 ";
    }

    GetProfile("WordIndex", n);

    if (n == 0) {
        cmdOpt += "-%I0 ";
    } /*else {

        cmdOpt += "-%I ";
    }*/
    /*
        GetProfile("HTMLFirst", n);

        if (n == 1) {
            cmdOpt += "-p7 ";
        }
    */
    QString filter;
    GetProfile("PrimaryScan", n);      // filter

    if (n >= 0 && n <= 2) {
        filter = "-p" + QString::number(n) + " ";
    } else if (n == 3) {   /* default */
        GetProfile("HTMLFirst", n1);

        if (!n1) {
            filter = "-p3 ";
        }
    } else {
        if (n == 4) {
            filter = "-p7 ";
        }
    }


    GetProfile("Travel", n);

    if (n == 0) {
        filter += "-S ";
    } else if (n == 1) {
        filter += "-D ";
    } else if (n == 2) {
        filter += "-U ";
    } else if (n == 3) {
        filter += "-B ";
    }

    //
    GetProfile("GlobalTravel", n);

    if (n == 0) {
        filter += "-a ";
    } else if (n == 1) {
        filter += "-d ";
    } else if (n == 2) {
        filter += "-l ";
    } else if (n == 3) {
        filter += "-e ";
    }

    //
    GetProfile("RewriteLinks", n);

    if (n == 0) {
        filter += "-K0 ";
    } else if (n == 1) {
        filter += "-K ";
    } else if (n == 2) {
        filter += "-K3 ";
    } else if (n == 3) {
        filter += "-K4 ";
    }

    cmdOpt += filter;

    //sizemax
    GetProfile("MaxAll", nf);

    if (n > 0) {
        cmdOpt += ("-M" + QString::number((qint64)(nf * 1024.0 * 1024.0)) + " ");
    }

    //pausebytes
    GetProfile("MaxWait", n);

    if (n > 0) {
        cmdOpt += ("-G" + QString::number(n) + " ");
    }

    float nf1;
    GetProfile("MaxHtml", nf);
    GetProfile("MaxOther", nf1);

    if (nf >= 0 ||  nf1 >= 0) {
        cmdOpt += "-m";

        if (nf1 > 0) {
            cmdOpt += QString::number((qint64)(nf1 * 1024.0 * 1024.0));
        } else {
            cmdOpt += "0";
        }

        if (nf > 0) {
            cmdOpt += ("," + QString::number((qint64)(nf * 1024.0 * 1024.0)));
        } else {
            cmdOpt += ",0";
        }

        cmdOpt += " ";
    }

    GetProfile("Sockets", n);

    if (n >= 0) {
        maxProgressRows = n;
    } else {
        maxProgressRows = 8;
    }

    cmdOpt += ("-c" + QString::number(n) + " ");

    GetProfile("TimeOut", n);

    if (n > 0) {
        cmdOpt += ("-T" + QString::number(n) + " ");
    }

    GetProfile("RateOut", n);

    if (n > 0) {
        cmdOpt += ("-J" + QString::number(n) + " ");
    }

    GetProfile("Retry", n);

    if (n > 0) {
        cmdOpt += ("-R" + QString::number(n) + " ");
    }

    int a = 0;
    GetProfile("RemoveTimeout", n);

    if (n > 0) {
        a += 1;
    }

    GetProfile("RemoveRateout", n);

    if (n > 0) {
        a += 2;
    }

    if (a > 0 ) {
        cmdOpt += ("-H" + QString::number(a) + " ");
    }

    GetProfile("KeepAlive", n);

    if (n > 0) {
        cmdOpt += "-%k ";
    } else {
        cmdOpt += "-%k0 ";
    }

    GetProfile("Log", n);

    if (n != 0) {
        cmdOpt += "-f2 ";
    } else {
        cmdOpt += "-Q ";
    }

    GetProfile("NoErrorPages", n);

    if (n > 0) {
        cmdOpt += "-o0 ";
    }

    //
    GetProfile("MaxTime", n);

    if (n > 0) {
        cmdOpt += ("-E" + QString::number(n) + " ");         // max timeout
    }

    GetProfile("MaxRate", nf);

    if (nf > 0.0) {
        cmdOpt += ("-A" + QString::number((int)(nf * 1024.0 * 1024.0)) + " ");     // max rate
    }

    //     maxConnections = 0;

    GetProfile("MaxConn", n);

    if (n > 0) {
        cmdOpt += ("-%c" + QString::number(n) + " ");      // max connections
        //         maxConnections = n;
    }

    GetProfile("MaxLinks", n);

    if (n > 0) {
        cmdOpt += ("-#L" + QString::number(n) + " ");         // max links
    }

    GetProfile("UseHTTPProxyForFTP", n);

    if (n > 0) {
        cmdOpt += "-%f ";
    } /*else {

        cmdOpt += "-%f0 ";
    }*/

    //     cmdOpt += "#f";  // flush

    QString os, br;
    GetProfile("BrowserID", st);
    GetProfile("BrowserName", br);
    GetProfile("osID", os);

    if (st.length() > 0) {
        if (st.contains("%s")) {
            st.replace("%s", os);
        }

        // qDebug() << "cmd" << st;
        cmdOpt += (" -F \"" + st + "\" ");
    }

    GetProfile("Footer", st);

    if (st.length() > 0) {
        QString stTmp = st;
        cmdOpt += (" -%F \"" + stTmp + "\" ");
    }

    GetProfile("Proxy", st);

    if (st.length() > 0) {
        GetProfile("ProxyPort", n);
        cmdOpt += (" -P " + st + ":" + QString::number(n) + " ");
    }

    // lang iso
    QString lng_iso = selectedLang;//translate("LANGUAGE_ISO");

    if (lng_iso.length() > 0) {
        cmdOpt +=  " -%l \"" + lng_iso;

        if (lng_iso != "en") {
            cmdOpt += ",en";
        }

        cmdOpt += "\" ";
    }

    // end of lang
    if (choixdeb == '!') {
        cmdOpt += " --testlinks ";
    } else if (choixdeb == 'Y') {
        cmdOpt += " --mirrorlinks ";
    }

    if (currentProject.length() > 0) {
        cmdOpt += " -O \"" + currentWorkDir + "/" + currentProject + "/\" ";
    }

    GetProfile("WildCardFilters", st);

    if (st.length() > 0) {
        cmdOpt += (" " + st);
    }

    for (int an = 1; an < 9; an++) {
        QString st1, st2;
        QString n1, n2;
        n1 = "MIMEDefsMime" + QString::number(an);
        n2 = "MIMEDefsExt" + QString::number(an);
        GetProfile(n1, st1);
        GetProfile(n2, st2);

        if (st1.length() > 0 && st2.length() > 0) {
            cmdOpt += (" --assume " + st2 + "=" + st1 + " ");
        }
    }

    // clean
    StripControls(cmdOpt);

    return cmdOpt;
}


void cOptions::initOptions()
{
    //     QMap<QString, OptStruct> m;
    prOptions[ "Near" ] = 1;
    prOptions[ "ParseAll" ] = 1;
    prOptions[ "Test" ] = 0;
    prOptions[ "HTMLFirst" ] = 0;
    prOptions[ "Dos" ] = 0;
    prOptions[ "ISO9660" ] = 0;
    prOptions[ "NoErrorPages" ] = 0;
    prOptions[ "NoExternalPages" ] = 1;
    prOptions[ "NoPwdInPages" ] = 0;
    prOptions[ "NoQueryStrings" ] = 1;
    prOptions[ "NoPurgeOldFiles" ] = 0;
    prOptions[ "Build" ] = 0;
    prOptions[ "WildCardFilters" ] = "+*.png +*.gif +*.jpg +*.css +*.js -ad.doubleclick.net/*";
    prOptions[ "Depth" ] = -1;
    prOptions[ "ExtDepth" ] = 0;
    prOptions[ "CurrentAction" ] = 0;
    prOptions[ "MaxHtml" ] = -1.0;
    prOptions[ "MaxOther" ] = -1.0;
    prOptions[ "MaxAll" ] = -1.0;
    prOptions[ "MaxWait" ] = 0;
    prOptions[ "MaxTime" ] = -1;
    prOptions[ "MaxRate" ] = -1.0;
    prOptions[ "MaxConn" ] = -1;
    prOptions[ "MaxLinks" ] = -1;
    prOptions[ "Proxy" ] = "";
    prOptions[ "CurrentUrl" ] = "";
    prOptions[ "CurrentURLList" ] = "";
    prOptions[ "Category" ] = "";
    prOptions[ "BuildString" ] = "%h%p/%n%q.%t";
    prOptions[ "UseHTTPProxyForFTP" ] = 0;
    prOptions[ "PrimaryScan" ] = 4;
    prOptions[ "Debug" ] = 0;
    prOptions[ "UserID" ] = "Mozilla/5.0 (X11; U; Linux i686; I; rv:17.0.1) Gecko/20100101 Firefox/17.0.1";
    prOptions[ "BrowserName" ] = "Firefox";
    prOptions[ "BrowserID" ] = "Mozilla/5.0 (%s; rv:17.0.1) Gecko/20100101 Firefox/17.0.1";
    prOptions[ "osID" ] = "X11; U; Linux i686; I";
    prOptions[ "Travel" ] = 1;
    prOptions[ "GlobalTravel" ] = 0;
    prOptions[ "RewriteLinks" ] = 0;
    prOptions[ "Footer" ] = "<!-- Mirrored from %s%s by HTTraQt Website Copier/1.x [Karbofos 2012-2023] %s -->";
    prOptions[ "Cache" ] = 1;
    prOptions[ "TimeOut" ] = -1;
    prOptions[ "Sockets" ] = 8;
    prOptions[ "Retry" ] = -1;
    prOptions[ "KeepAlive" ] = 1;
    prOptions[ "RemoveTimeout" ] = 0;
    prOptions[ "RemoveRateout" ] = 0;
    prOptions[ "RateOut" ] = 0;
    prOptions[ "StoreAllInCache" ] = 0;

    for (int j = 1; j <= 8; ++j) {
        QString n;
        n = "MIMEDefsMime" + QString::number(j);
        prOptions[ n ] = "";
        n = "MIMEDefsExt" + QString::number(j);
        prOptions[ n ] = "";
    }

    prOptions[ "NoRecatch" ] = 0;
    prOptions[ "Log" ] = 0;
    prOptions[ "Index" ] = 1;
    prOptions[ "BuildTopIndex" ] = 1;
    prOptions[ "WordIndex" ] = 1;
    prOptions[ "LogType" ] = 0;
    prOptions[ "URLHack" ] = 1;
    prOptions[ "Cookies" ] = 1;
    prOptions[ "UpdateHack" ] = 1;
    prOptions[ "ParseJava" ] = 1;
    prOptions[ "TolerantRequests" ] = 0;
    prOptions[ "HTTP10" ] = 0;
    prOptions[ "CheckType" ] = 0;
    prOptions[ "FollowRobotsTxt" ] = 2;
    prOptions[ "ProxyPort" ] = 21;
}




