/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QDir>

#include "includes/translator.h"
#include "includes/httraqt.h"

extern HTTraQt* mainWidget;

QVector<QString> cTranslator::translateTable;


QString cTranslator::engText[] = {
    "HTTraQt",   // 0
    "N# connections", // 1
    "Select font size", // 2
    "TimeOut", // 3
    "Retries", // 4
    "Maximum transfer rate", // 5
    "Minimum transfer rate", // 6
    "Persistent connections (Keep-Alive)", // 7
    "Cancel all links from host if timeout occurs", // 8
    "Cancel all links from host if too slow", // 9
    "Check document type", // 10
    "Spider", // 11
    "Accept cookies", // 12
    "Parse java files", // 13
    "Update hack (limit re-transfers)", // 14
    "URL hacks (join similar URLs)", // 15
    "Tolerant requests (for servers)", // 16
    "Force old HTTP/1.0 requests (no 1.1)", // 17
    "Browser identity", // 18
    "Operation system Identity", // 19
    "Operation", // 20
    "HTML footer", // 21
    "Structure type (how links are saved)", // 22
    "DOS names (8+3)", // 23
    "No error pages", // 24
    "No external pages", // 25
    "Hide passwords", // 26
    "Hide query strings", // 27
    "Do not purge old files", // 28
    "ISO9660 names (CDROM)", // 29
    "Build", // 30
    "These options should be modified only exceptionally", // 31
    "Primary Scan Rule", // 32
    "Travel mode", // 33
    "Global travel mode", // 34
    "Rewrite links: internal / external", // 35
    "Use a cache for updates and retries", // 36
    "Activate Debugging Mode (httraqt.log)", // 37
    "Maximum mirroring depth from root address", // 38
    "Maximum mirroring depth for external/forbidden addresses (0, that is, none, is the default)", // 39
    "Max size of any HTML file", // 40
    "Max size of any non-HTML file", // 41
    "Size limit", // 42
    "Pause after downloading..", // 43
    "Max time", // 44
    "Size", // 45
    "Max connections / seconds", // 46
    "Maximum number of links", // 47
    "All links will match", // 48
    "Get non-HTML files related to a link, eg external .ZIP or pictures", // 49
    "Test all links (even forbidden ones)", // 50
    "Get HTML files first!", // 51
    "Type/MIME associations", // 52
    "File types:", // 53
    "MIME identity:", // 54
    "Proxy address:", // 55
    "Proxy port:", // 56
    "Login", // 57
    "Password", // 58
    "Use proxy for ftp transfers", // 59
    "Use wildcards to exclude or include URLs or links.\nYou can put several scan strings on the same line.\nUse spaces as separators.\n\nExample: +*.zip -www.*.com -www.*.edu/cgi-bin/*.cgi", // 60
    "Criterion", // 61
    "String", // 62
    "Tip: To have ALL GIF files included, use something like +www.someweb.com/*.gif. \n(+*.gif / -*.gif will include/exclude ALL GIFs from ALL sites)", // 63
    "WildCardFilters", // 64
    "Add Scan Rule", // 65
    "Exclude links", // 66
    "Include link(s)", // 67
    "Store ALL files in cache", // 68
    "Do not re-download locally erased files", // 69
    "Create Log files", // 70
    "Make an index", // 71
    "Make a word database", // 72
    "File names with extension:\nFile names containing:\nThis file name:\nFolder names containing:\nThis folder name:\nLinks on this domain:\nLinks on domains containing:\nLinks from this host:\nLinks containing:\nThis link:\nALL LINKS", // 73
    "Show all\nHide debug\nHide infos\nHide debug and infos", // 74
    "Site-structure (default)\nHtml in web/,       images/other files in web/images/\nHtml in web/html,   images/other in web/images\nHtml in web/,       images/other in web/\nHtml in web/,       images/other in web/xxx, where xxx is the file extension\nHtml in web/html,   images/other in web/xxx\nSite-structure, without www.domain.xxx/\nHtml in site_name/, images/other files in site_name/images/\nHtml in site_name/html, images/other in site_name/images\nHtml in site_name/, images/other in site_name/\nHtml in site_name/, images/other in site_name/xxx\nHtml in site_name/html, images/other in site_name/xxx\nAll files in web/, with random names (gadget !)\nAll files in site_name/, with random names (gadget !)\nUser-defined structure..", // 75
    "Just scan\nStore html files\nStore non html files\nStore all files (default)\nStore html files first", // 76
    "Stay in the same directory\nCan go down (default)\nCan go up\nCan both go up & down", // 77
    "Stay on the same address (default)\nStay on the same domain\nStay on the same top level domain\nGo everywhere on the web", // 78
    "Never\nIf unknown (except /)\nIf unknown", // 79
    "no robots.txt rules\nrobots.txt except wizard\nfollow robots.txt rules", // 80
    "normal\nextended\ndebug", // 81
    "Download web site(s)\nDownload web site(s) + questions\nGet individual files\nDownload all sites in pages (multiple mirror)\nTest links in pages (bookmark test)\n* Continue interrupted download\n* Update existing download", // 82
    "Relative URI / Absolute URL (default)\nAbsolute URL / Absolute URL\nAbsolute URI / Absolute URL\nOriginal URL / Original URL", // 83
    "- Mirroring Mode -\n\nEnter address(es) in URL box", // 84
    "- Interactive Wizard Mode (questions) -\n\nEnter address(es) in URL box", // 85
    "- File Download Mode -\n\nEnter file address(es) in URL box", // 86
    "- Links List Mode -\n\nUse URL box to enter address(es) of page(s) containing links to mirror", // 87
    "- Link Testing Mode -\n\nEnter Web address(es) with links to test in URL box", // 88
    "- Update Mode -\n\nVerify address(es) in URL box, check parameters if necessary then click on 'NEXT' button", // 89
    "- Resume Mode (Interrupted Operation) -\n\nVerify address(es) in URL box, check parameters if necessary then click on 'NEXT' button", // 90
    "Ok", // 91
    "Cancel", // 92
    "Exit", // 93
    "Close", // 94
    "Cancel changes", // 95
    "Click to confirm", // 96
    "Click to get help!", // 97
    "Click to return to previous screen", // 98
    "Click to go to next screen", // 99
    "Hide password", // 100
    "Save project", // 101
    "Close current project?", // 102
    "Delete this project?", // 103
    "Delete empty project %s?", // 104
    "Action not yet implemented", // 105
    "Error deleting this project", // 106
    "Select a rule for the filter", // 107
    "Enter keywords for the filter", // 108
    "Add this rule", // 109
    "Please enter one or several keyword(s) for the rule", // 110
    "Add", // 111
    "Scan Rules", // 112
    "Matching links will be excluded:", // 113
    "Matching links will be included:", // 114
    "Example:", // 115
    "gif\nWill match all GIF files", // 116
    "blue\nWill find all files with a matching 'blue' sub-string such as 'bluesky-small.jpeg'", // 117
    "bigfile.mov\nWill match the file 'bigfile.mov', but not 'bigfile2.mov'", // 118
    "cgi\nWill find links with folder name matching sub-string 'cgi' such as /cgi-bin/somecgi.cgi", // 119
    "cgi-bin\nWill find links with folder name matching whole 'cgi-bin' string (but not cgi-bin-2, for example)", // 120
    "someweb.com\nWill find links with matching sub-string such as www.someweb.com, private.someweb.com etc.", // 121
    "someweb\nWill find links with matching folder sub-string such as www.someweb.com, www.someweb.edu, private.someweb.otherweb.com etc.", // 122
    "www.someweb.com\nWill find links matching whole 'www.someweb.com' sub-string (but not links such as private.someweb.com/..)", // 123
    "someweb\nWill find any links with matching sub-string such as www.someweb.com/.., www.test.abc/fromsomeweb/index.html, www.test.abc/test/someweb.html etc.", // 124
    "www.test.com/test/someweb.html\nWill only find the 'www.test.com/test/someweb.html' file. Note that you have to type the complete path (URL + site path)", // 125
    "Add exclusion filter", // 126
    "Add inclusion filter", // 127
    "Existing filters", // 128
    "Cancel changes", // 129
    "Save current preferences as default values", // 130
    "Click to confirm", // 131
    "No log files in %s!", // 132
    "No 'index.html' file in %s!", // 133
    "Click to quit HTTraQt Website Copier", // 134
    "Browse HTML start page", // 135
    "End of mirror", // 136
    "View log files", // 137
    "Browse Mirrored Website", // 138
    "New project...", // 139
    "View error and warning reports", // 140
    "View report", // 141
    "Close the log file window", // 142
    "Info type:", // 143
    "Infos", // 144
    "Find", // 145
    "Find a word", // 146
    "Info log file", // 147
    "Warning/Errors log file", // 148
    "Unable to initialize the OLE system", // 149
    "HTTraQt could not find any interrupted download file cache in the specified folder!", // 150
    "Could not connect to provider", // 151
    "receive", // 152
    "request", // 153
    "connect", // 154
    "search", // 155
    "Ready", // 156
    "ready", // 157
    "waiting", // 158
    "error", // 159
    "Receiving files..", // 160
    "Parsing HTML file..", // 161
    "Purging files..", // 162
    "Loading cache in progress..", // 163
    "Parsing HTML file (testing links)..", // 164
    "Pause - Toggle [Mirror]/[Pause download] to resume operation", // 165
    "Paused (select [File]/[Pause transfer] to continue)", // 166
    "Finishing pending transfers - Select [Cancel] to stop now!", // 167
    "scanning", // 168
    "Waiting for scheduled time..", // 169
    "Transferring data..", // 170
    "Connecting to provider", // 171
    "[%d seconds] to go before start of operation", // 172
    "Site mirroring in progress [%s, %s]", // 173
    "Site mirroring finished!", // 174
    "A problem occured during the mirroring operation\n", // 175
    "\nDuring:\n", // 176
    "\nSee the log file if necessary.\n\nClick FINISH to quit HTTraQt Website Copier.\n\nThanks for using HTTraQt!", // 177
    "Mirroring operation complete.\nClick Exit to quit HTTraQt.\nSee log file(s) if necessary to ensure that everything is OK.\n\nThanks for using HTTraQt!", // 178
    "* * MIRROR ABORTED! * *\nThe current temporary cache is required for any update operation and only contains data downloaded during the present aborted session.\nThe former cache might contain more complete information; if you do not want to lose that information, you have to restore it and delete the current cache.\n[Note: This can easily be done here by erasing the hts-cache/new.* files]\n\nDo you think the former cache might contain more complete information, and do you want to restore it?", // 179
    "* * MIRROR ERROR! * *\nHTTraQt has detected that the current mirror is empty. If it was an update, the previous mirror has been restored.\nReason: the first page(s) either could not be found, or a connection problem occured.\n=> Ensure that the website still exists, and/or check your proxy settings! <=", // 180
    "\n\nTip: Click [View log file] to see warning or error messages", // 181
    "Error deleting a hts-cache/new.* file, please do it manually", // 182
    "Do you really want to quit HTTraQt Website Copier?", // 183
    "Log files Path", // 184
    "New project / Import?", // 185
    "Choose criterion", // 186
    "Maximum link scanning depth", // 187
    "Enter address(es) here", // 188
    "Define additional filtering rules", // 189
    "Proxy Name (if needed)", // 190
    "Proxy Port", // 191
    "Define proxy settings", // 192
    "Use standard HTTP proxy as FTP proxy", // 193
    "Path", // 194
    "Select Path", // 195
    "Quit HTTraQt Website Copier", // 196
    "About HTTraQt", // 197
    "Save current preferences as default values", // 198
    "Click to continue", // 199
    "Click to define options", // 200
    "Click to add a URL", // 201
    "Load URL(s) from text file", // 202
    "HTTraQt preferences (*.opt)|*.opt||", // 203
    "Address List text file (*.txt)|*.txt||", // 204
    "File not found!", // 205
    "Do you really want to change the project name/path?", // 206
    "Load user-default options?", // 207
    "Save user-default options?", // 208
    "Reset all default options?", // 209
    "Welcome to HTTraQt!", // 210
    "Action:", // 211
    "Max Depth", // 212
    "Maximum external depth:", // 213
    "Filters (refuse/accept links) :", // 214
    "Paths", // 215
    "Define..", // 216
    "Set options..", // 217
    "Preferences and mirror options:", // 218
    "Project name", // 219
    "Add a URL...", // 220
    "Web Addresses: (URL)", // 221
    "Stop HTTraQt?", // 222
    "Hibernate", // 223
    "Pause Download?", // 224
    "Pause", // 225
    "Stop the mirroring operation", // 226
    "Minimize to System Tray", // 227
    "Click to skip a link or stop parsing", // 228
    "Click to skip a link", // 229
    "Bytes saved", // 230
    "Stop", // 231
    "Links scanned", // 232
    "Time:", // 233
    "Connections:", // 234
    "Running:", // 235
    "Hide", // 236
    "Transfer rate", // 237
    "SKIP", // 238
    "Information", // 239
    "Files written:", // 240
    "Files updated:", // 241
    "Errors:", // 242
    "Errors", // 243
    "In progress:", // 244
    "Follow external links", // 245
    "Test all links in pages", // 246
    "Try to ferret out all links", // 247
    "Download HTML files first (faster)", // 248
    "Choose local site structure", // 249
    "Set user-defined structure on disk", // 250
    "Do not update zero size or user-erased files", // 251
    "Create a Start Page", // 252
    "Create a word database of all html pages", // 253
    "Create error logging and report files", // 254
    "Generate DOS 8-3 filenames ONLY", // 255
    "Generate ISO9660 filenames ONLY for CDROM medias", // 256
    "Do not create HTML error pages", // 257
    "Select file types to be saved to disk", // 258
    "Select parsing direction", // 259
    "Select global parsing direction", // 260
    "Setup URL rewriting rules for internal links (downloaded ones) and external links (not downloaded ones)", // 261
    "Max simultaneous connections", // 262
    "File timeout", // 263
    "Minimum admissible transfer rate", // 264
    "Maximum number of retries on non-fatal errors", // 265
    "Maximum size for any single HTML file", // 266
    "Maximum size for any single non-HTML file", // 267
    "Maximum amount of bytes to retrieve from the Web", // 268
    "Make a pause after downloading this amount of bytes", // 269
    "Maximum duration time for the mirroring operation", // 270
    "Maximum connections/seconds (avoid server overload)", // 271
    "Maximum number of links that can be tested (not saved!)", // 272
    "Comment to be placed in each HTML file", // 273
    "Back to starting page", // 274
    "Save current preferences as default values", // 275
    "Click to continue", // 276
    "Click to cancel changes", // 277
    "Follow local robots rules on sites", // 278
    "Links to non-localised external pages will produce error pages", // 279
    "Do not erase obsolete files after update", // 280
    "Accept cookies?", // 281
    "Check document type when unknown?", // 282
    "Parse java applets to retrieve included files that must be downloaded?", // 283
    "Store all files in cache instead of HTML only", // 284
    "Log file type (if generated)", // 285
    "Create a debugging file", // 286
    "Use non-standard requests to get round some server bugs", // 287
    "Use old HTTP/1.0 requests (limits engine power!)", // 288
    "Attempt to limit retransfers through several tricks (file size test..)", // 289
    "Attempt to limit the number of links by skipping similar URLs (www.foo.com==foo.com, http=https ..)", // 290
    "Write external links without login/password", // 291
    "Write internal links without query string", // 292
    "Try to catch all URLs (even in unknown tags/code)", // 293
    "Use a cache for updates", // 294
    "Log files", // 295
    "Primary Scan Rule", // 296
    "Flow control", // 297
    "Limits", // 298
    "Identity", // 299
    "Abandon host if error", // 300
    "Abandon host if too slow", // 301
    "Configure", // 302
    "Reduce connection time and type lookup time using persistent connections", // 303
    "Max site size", // 304
    "Save prefs", // 305
    "Save", // 306
    "Max transfer rate", // 307
    "Follow robots.txt", // 308
    "Links", // 309
    "Experts Only", // 310
    "Flow Control", // 311
    "Browser ID", // 312
    "Scan Rules", // 313
    "Log, Index, Cache", // 314
    "Proxy", // 315
    "MIME Types", // 316
    "Do not connect to a provider (already connected)", // 317
    "Do not use remote access connection", // 318
    "Schedule the mirroring operation", // 319
    "Click to start!", // 320
    "No saved password for this connection!", // 321
    "Can not get remote connection settings", // 322
    "Select a connection provider", // 323
    "Start", // 324
    "Please adjust connection parameters if necessary,\nthen press FINISH to launch the mirroring operation.", // 325
    "Save settings only, do not launch download now.", // 326
    "On hold", // 327
    "Shutdown", // 328
    "Transfer scheduled for: (hh/mm/ss)", // 329
    "Connect to provider (RAS)", // 330
    "Connect to this provider", // 331
    "Disconnect when finished", // 332
    "Disconnect modem on completion", // 333
    "\n(Please notify us of any bug or problem)\n\nDevelopment:\nInterface (Windows): Xavier Roche\n"
    "Spider: Xavier Roche\nJavaParserClasses: Yann Philippot\n\n(C)1998-2003 Xavier Roche and other contributors\n"
    "MANY THANKS for translation tips to:\nRobert Lagadec (rlagadec@yahoo.fr)", // 334
    "About HTTraQt Website Copier", // 335
    "Please visit our Web page", // 336
    "Wizard query", // 337
    "Your answer:", // 338
    "Link detected..", // 339
    "Choose a rule", // 340
    "Ignore this link", // 341
    "Ignore directory", // 342
    "Ignore domain", // 343
    "Catch this page only", // 344
    "Mirror site", // 345
    "Mirror domain", // 346
    "Ignore all", // 347
    "Wizard query", // 348
    "No", // 349
    "Options", // 350
    "Pause transfer", // 351
    "Modify options", // 352
    "View log", // 353
    "View error log", // 354
    "View file transfers", // 355
    "Check program updates...", // 356
    "&Toolbar", // 357
    "&Status Bar", // 358
    "S&plit", // 359
    "File", // 360
    "Progress", // 361
    "Preferences", // 362
    "Mirror", // 363
    "Log", // 364
    "Window", // 365
    "Help", // 366
    "Load default options", // 367
    "Save default options", // 368
    "Reset to default options", // 369
    "Load options...", // 370
    "Save options as...", // 371
    "Language preference...", // 372
    "Contents...", // 373
    "About HTTraQt...", // 374
    "New project\tCtrl+N", // 375
    "&Open...\tCtrl+O", // 376
    "&Save\tCtrl+S", // 377
    "Save &As...", // 378
    "&Delete...", // 379
    "&Browse sites...", // 380
    "User-defined structure", // 381
    "%n\tName of file without file type (ex: image)\n%N\tName of file including file type (ex: image.gif)\n"
    "%t\tFile type only (ex: gif)\n%p\tPath [without ending /] (ex: /someimages)\n"
    "%h\tHost name (ex: www.someweb.com)\n%M\tMD5 URL (128 bits, 32 ascii bytes)\n"
    "%Q\tMD5 query string (128 bits, 32 ascii bytes)\n%q\tMD5 small query string (16 bits, 4 ascii bytes)\n"
    "\n%s?\tShort name (ex: %sN)", // 382
    "Example:\t%h%p/%n%q.%t\n->\t\t%HOME%/mirror/www.someweb.com/someimages/image.gif", // 383
    "Proxy settings", // 384
    "Authentication (only if needed)", // 385
    "Enter proxy address here", // 386
    "Enter proxy port here", // 387
    "Enter proxy login", // 388
    "Enter proxy password", // 389
    "Enter project name here", // 390
    "Enter saving path here", // 391
    "Select existing project to update", // 392
    "Click here to select path", // 393
    "Select or create a new category name, to sort your mirrors in categories", // 394
    "HTTraQt Project Wizard...", // 395
    "New project name:", // 396
    "Existing project name:", // 397
    "Project name:", // 398
    "Base path:", // 399
    "Project category:", // 400
    "%HOME%/My Web Sites", // 401
    "Type a new project name, \nor select existing project to update/resume", // 402
    "New project", // 403
    "Insert URL", // 404
    "URL:", // 405
    "Authentication (only if needed)", // 406
    "Forms or complex links:", // 407
    "Capture URL...", // 408
    "Enter URL address(es) here", // 409
    "Enter site login", // 410
    "Enter site password", // 411
    "Use this capture tool for links that can only be accessed through forms or javascript code", // 412
    "Choose language according to preference", // 413
    "Catch URL!", // 414
    "Please set temporary browser proxy settings to the following values (Copy/Paste Proxy Address and Port).\n"
    "Then click on the Form SUBMIT button in your browser page, or click on the specific link you want to capture.", // 415
    "This will send the desired link from your browser to HTTraQt.", // 416
    "ABORT", // 417
    "Copy/Paste the temporary proxy parameters here", // 418
    "Unable to find Help files!", // 419
    "Unable to save parameters!", // 420
    "Please drag only one folder at a time", // 421
    "Please drag only folders, not files", // 422
    "Please drag folders only", // 423
    "Select user-defined structure?", // 424
    "Please ensure that the user-defined-string is correct,\notherwise filenames will be bogus!", // 425
    "Do you really want to use a user-defined structure?", // 426
    "Too manu URLs, cannot handle so many links!!", // 427
    "Not enough memory, fatal internal error..", // 428
    "Unknown operation!", // 429
    "Add this URL?\n", // 430
    "Warning: main process is still not responding, cannot add URL(s)..", // 431
    "Select or modify your file type(s) here", // 432
    "Select or modify your MIME type(s) here", // 433
    "Go up", // 434
    "Go down", // 435
    "File download information", // 436
    "Freeze Window", // 437
    "More information:", // 438
    "Welcome to HTTraQt Website Copier!\n\nPlease click on the NEXT button to\n"
    "\n- start a new project\n- or resume a partial download", // 439
    "Open Source offline browser", // 440
    "Website Copier/Offline Browser. Copy remote websites to your computer. Free.", // 441
    "httrack, httraqt, webhttrack, offline browser", // 442
    "URL list (*.txt)", // 443
    "Previous", // 444
    "Next", // 445
    "URLs", // 446
    "Warning", // 447
    "Your browser does not currently support javascript. For better results, please use a javascript-aware browser.", // 448
    "Thank you", // 449
    "You can now close this window", // 450
    "Server terminated", // 451
    "A fatal error has occured during this mirror", // 452
    "Wrong URL(s)!", // 453
    "How You can thank a developer:", // 454
    "Share the link to this program or to this project", // 455
    "Report to developer about bugs or mistakes in the program", // 456
    "Donate", // 457
    "\n(Please notify us of any bug or problem)\n\nDevelopment:\n"
    "Interface (Windows): Xavier Roche\nInterface (Qt4/Qt5 based): Eduard Kalinowski\nSpider: Xavier Roche\n"
    "JavaParserClasses: Yann Philippot\n\n(C)1998-2003 Xavier Roche and other contributors\n"
    "MANY THANKS for translation tips to:\nRobert Lagadec (rlagadec@yahoo.fr)", // 458
    "Get from the clipboard", // 459
    "Documents", // 460
    "Archives", // 461
    "Images", // 462
    "Multimedia", // 463
    "Open new GUI", // 464
    "Browse exists projects", // 465
    "Step by step...", // 466
    "To the project page...", // 467
    "About HTTraQt Website Copier...", //468
    "About Qt", // 469
    "Question", // 470
    "Open File", // 471
    "No URL!", // 472
    "The URL list is not complete!", // 473
    "File error", // 474
    "remove", // 475
    "are you sure?", // 476
    "Any subdirectory not empty!", // 477
    "Save File", // 478
    "&Language", // 479
    "Stop?", // 480
    "No directory", // 481
    "Creating error", // 482
    "Can not create directory!", // 483
    "No project", // 484
    "Waiting for specific hour to start", // 485
    "Mirror waiting [%d seconds]", // 486
    "Error", // 487
    "Yes", // 488
    "Build top Index", // 489
    "Units", // 490
    "Open", // 491
    "PC off when finished", // 492
    "Shutdown counter (minutes)", // 493
    "Make or correct translation", // 494
    "Browser name", // 495
    ""
};


bool cTranslator::loadTranslation(const QString fname)
{
    QFile langFile(fname);
    short vecSize = (short)(sizeof(engText) / sizeof(char*));
    translateTable.clear();
    translateTable.resize(vecSize + 1);

    if (!langFile.open(QIODevice::ReadOnly)) {
        return false;
    }

    //     qDebug() << fname;

    QTextStream stream(&langFile);
#if USE_QT_VERSION == 6
    stream.setEncoding(QStringConverter::Utf8);
#else
    stream.setCodec("UTF-8");
#endif

    QString llEng = stream.readLine();

    if (llEng.length() > 0) {
        llEng.remove("\n");
    }

    if (llEng == "LANGUAGE_NAME") {
        translateTable << llEng;
    } else {
        MessageBox::exec(this, translate(_FILEERR), "File is not a language file!", QMessageBox::Warning);

        langFile.close();
        return (false);
    }

    bool begTranslation = false;

    while (!stream.atEnd()) {
        llEng = stream.readLine();
        llEng = convertString(llEng);
#if USE_QT_VERSION == 6
        QRegularExpression r = QRegularExpression("t[0-9]{3}=");
#else
        QRegExp r = QRegExp("t[0-9]{3}=");
#endif

        if (llEng.indexOf(r) == 0) {
            int num, pos;
            QString str;
            pos = llEng.indexOf("=");

            if (pos > 0) {
                str = llEng.mid(pos + 1);
                //
                num = llEng.mid(1, pos - 1).toShort();

                //                     qDebug() << num << str;
                if (num >= vecSize) {
                    qDebug() << "size too big";
                } else {
                    translateTable[num] = str;
                }
            }
        } else {
            if (llEng == "Ok") { // translation part beginns with "Ok"
                begTranslation = true;
            }

            if (begTranslation == false) {
                continue;
            }

            bool found = false;
            QString llTranslate = stream.readLine();

            if (llTranslate.length() == 0) {
                continue;
            }

            llTranslate = convertString(llTranslate);

            for(int i = 0; i < vecSize; i++) { // suche
                QString tmpEng = engText[i];

                if (tmpEng.indexOf("%HOME%") >= 0) {
                    tmpEng.replace("%HOME%", QDir::homePath());
                }

                if ( tmpEng == llEng) {
                    translateTable[i] = llTranslate;
                    found = true;
                    break;
                }
            }

            if (found == false) {
                qDebug() << "no translation found" << llEng;
            }

        }
    }

    langFile.close();

    return (true);
}


QString cTranslator::convertString(const QString &s)
{
    QString res = s;

    if (res.length() > 0) {
        res.replace( "\\r\\n", "\n");
        res.replace( "\\n", "\n");
        res.replace( "\\t", "\t");
        res.replace( "\\\\", "/");
        res.replace( "&amp;", "&");
        res.replace( "&gt;", ">");
        res.replace( "&lt;", "<");
    }

    if (res.length() == 0) {
        return res;
    }

    if (res.indexOf("winhttrack") >= 0) {
        res.replace("winhttrack", "httraqt");
    }

    if (res.indexOf("C:/") >= 0) {
        res.replace("C:/", "%HOME%/");
    }

    if (res.indexOf("c:/") >= 0) {
        res.replace("c:/", "%HOME%/");
    }

    if (res.indexOf("%HOME%") >= 0) {
        res.replace("%HOME%", QDir::homePath());
    }

    return res;
}


const QString cTranslator::translate( int id )
{
    QString m;

    if (id < 0) {
        return "";
    }

    if (translateTable.size() == 0) {
        return QString(engText[id]);
    }

    if ((id >= 0) && (id < (int)(sizeof(engText) / sizeof(char*)))) {
        if(translateTable[id].length() == 0) {
            m = QString(engText[id]);
        } else {
            m = translateTable[id];
        }

        if (m.indexOf("%HOME%") >= 0) {
            m.replace("%HOME%", QDir::homePath());
        }
    }

    return m;
}




