/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef OPTIONSFLOW_H
#define OPTIONSFLOW_H

#include <QWidget>
#include <QString>

#include "./OptionsDialog.h"
#include "ui_OptionsFlow.h"

class OptionsDialog;

class optionsFlow : public QWidget, private Ui::flowForm
{
        Q_OBJECT

    public:
        optionsFlow(QWidget* parent = 0, Qt::WindowFlags fl = Qt::WindowType::Widget);
        ~optionsFlow();
        /*$PUBLIC_FUNCTIONS$*/
    private:
        void initTextPoints();

    public slots:
        /*$PUBLIC_SLOTS$*/

    protected:
        void selectOnCombo(QComboBox &cb, QStringList &sl, QString vari);

    protected:
        /*$PROTECTED_FUNCTIONS$*/
        OptionsDialog* parent;

    protected slots:
        /*$PROTECTED_SLOTS$*/
        void editingFinished(void);

    protected slots:
        /*$PROTECTED_SLOTS$*/
    private:
        QVector<trWidgets>* opts;
};

#endif

