/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef OPTIONSMIME_H
#define OPTIONSMIME_H

#include <QWidget>
#include <QString>

#include "ui_OptionsMime.h"
#include "./OptionsDialog.h"

class OptionsDialog;

class optionsMime : public QWidget, private Ui::mimeForm
{
        Q_OBJECT

    public:
        optionsMime(QWidget* parent = 0, Qt::WindowFlags fl = Qt::WindowType::Widget);
        ~optionsMime();
        /*$PUBLIC_FUNCTIONS$*/
    private:
        void initTextPoints();

    public slots:
        /*$PUBLIC_SLOTS$*/

    protected:
        void selectOnCombo(QComboBox &cb, QStringList &sl, QString vari);

    protected:
        /*$PROTECTED_FUNCTIONS$*/
        OptionsDialog* parent;
        QVector<QComboBox*> ident;
        QVector<QComboBox*> mime;

    protected slots:
        void onMIME();
        /*$PROTECTED_SLOTS$*/
    private:
        QVector<trWidgets>* opts;
};

#endif

