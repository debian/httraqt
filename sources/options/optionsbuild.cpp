/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/BuildStringDialog.h"
#include "includes/OptionsDialog.h"
#include "includes/optionsbuild.h"
#include "../main/includes/httraqt.h"

optionsBuild::optionsBuild(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::buildForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    QString str = this->parent->translate(LISTDEF_3);
    QStringList strl = str.split("\n");

    buildForm::label1045->insertItems(0, strl);

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();

    connect(buildForm::label1209, SIGNAL(clicked()), this, SLOT(callBuildStringDialog()));
}


optionsBuild::~optionsBuild()
{
}


void optionsBuild::initTextPoints()
{
    *opts << (trWidgets) {
        buildForm::label1192, _STRUCT_TYPE, "", LABEL, 0
    };
    *opts << (trWidgets) {
        buildForm::label1030, _DOS_NAMES, "Dos", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1034, _NO_ERR_PAG, "NoErrorPages", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1035, _NO_EXTERAL, "NoExternalPages", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1037, _HIDE_PASS, "NoPwdInPages", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1038, _HIDE_QUERY, "NoQueryStrings", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1036, _NOT_PURGE, "NoPurgeOldFiles", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1031, _ISO9660_NAMES, "ISO9660", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        buildForm::label1045, -1, "Build", COMBOBOX, 0
    };
}


void optionsBuild::callBuildStringDialog()
{
    // einbauen aufruf vom dialog
    BuildStringDialog* bdial = new BuildStringDialog(this->parent->parent);
    bdial->exec();
}

/*$SPECIALIZATION$*/


