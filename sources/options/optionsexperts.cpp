/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionsexperts.h"
#include "../main/includes/httraqt.h"


optionsExperts::optionsExperts(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::expertsForm()
{
    QString t;
    QStringList sl;

    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    t = this->parent->translate(LISTDEF_4);
    sl = t.split("\n");
    expertsForm::label1046->addItems(sl);

    t = this->parent->translate(LISTDEF_5);
    sl = t.split("\n");
    expertsForm::label1047->addItems(sl);

    t = this->parent->translate(LISTDEF_6);
    sl = t.split("\n");
    expertsForm::label1048->addItems(sl);

    t = this->parent->translate(LISTDEF_11);
    sl = t.split("\n");
    expertsForm::label1049->addItems(sl);

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}


optionsExperts::~optionsExperts()
{
}


void optionsExperts::initTextPoints()
{
    *opts << (trWidgets) {
        expertsForm::label1257, _ONLY_EXPERTS, "", LABEL, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1193, _PRIM_SCAN, "", LABEL, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1200, _TRAVEL_MODE, "", LABEL, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1201, _GLOBAL_TRAVEL, "", LABEL, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1202, _REWRITE_LINKS, "", LABEL, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1046, -1, "PrimaryScan", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1047, -1, "Travel", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1048, -1, "GlobalTravel", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1049, -1, "RewriteLinks", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1029, _USE_CACHE, "Cache", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        expertsForm::label1021_2, _DEBUG_MODE, "Debug", CHECKBOX, 0
    };
}


/*$SPECIALIZATION$*/


