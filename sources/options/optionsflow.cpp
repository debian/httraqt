/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/optionsflow.h"
#include "includes/OptionsDialog.h"
#include "../main/includes/httraqt.h"



optionsFlow::optionsFlow(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::flowForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    QStringList connections;
    connections << "";

    for (int i = 1; i < 15; i++) {
        connections << QString::number(i);
    }

    flowForm::labelConn->insertItems(0, connections);

    QStringList timeout;
    timeout << "" << "30" << "60" << "120" << "180" << "300" << "600" << "1200";
    flowForm::labelTimeout->insertItems(0, timeout);

    flowForm::labelTimeout->setDuplicatesEnabled(true);

    connect (flowForm::labelTimeout->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    QStringList retries;
    retries << "" << "0" << "1" << "2" << "3" << "6";
    flowForm::labelRetries->insertItems(0, retries);
    flowForm::labelRetries->setDuplicatesEnabled(true);

    connect (flowForm::labelRetries->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}


optionsFlow::~optionsFlow()
{
}


// check the entered value depended from combobox
void optionsFlow::editingFinished()
{
    QLineEdit* from;
    bool valGood;
    QString t;
    from = (QLineEdit*)sender();
    QComboBox *par = (QComboBox*)from->parent();
    t = from->text();

    //     qDebug() << from << t;
    if (t == "" || t == "\n") {
        par->setCurrentIndex(0); // first element, = -1
        return;
    }

    disconnect (from, SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    if (from == flowForm::labelTimeout->lineEdit()) { // int, range: space, 0..255
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 1200)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (from == flowForm::labelRetries->lineEdit()) { // int, range: space, 0..9999
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 99)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (valGood == true) {
        //         qDebug() << "right" << t;
        int currInd = par->findText(t);

        if ( currInd < 0) {
            par->addItem(t);
            par->model()->sort(0);
            int tInd = par->findText(t);
            par->setCurrentIndex(tInd);
        } else {
            par->setCurrentIndex(currInd);
        }
    }

    connect (from, SIGNAL(editingFinished()), this, SLOT(editingFinished()));
}


void optionsFlow::initTextPoints()
{
    *opts << (trWidgets) {
        flowForm::label1201_2, _NR_CONN, "", LABEL,  0
    };
    *opts << (trWidgets) {
        flowForm::label1290, _TIMEOUTS, "", LABEL,  0
    };
    *opts << (trWidgets) {
        flowForm::label1203, _RETRIES, "", LABEL,  0
    };
    *opts << (trWidgets) {
        flowForm::label1202_2, _MIN_RATE, "", LABEL,  0
    };
    *opts << (trWidgets) {
        flowForm::label1055, -1, "RateOut", EDITLINE,  0
    };
    *opts << (trWidgets) {
        flowForm::labelConn, -1, "Sockets", COMBOBOX,  0
    };
    *opts << (trWidgets) {
        flowForm::labelTimeout, -1, "TimeOut", COMBOBOX,  0
    };
    *opts << (trWidgets) {
        flowForm::labelRetries, -1, "Retry", COMBOBOX,  0
    };
    *opts << (trWidgets) {
        flowForm::label1033, _KEEP_ALIVE, "KeepAlive", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        flowForm::label1031_2, _CANCEL_TIMEOUT, "RemoveTimeout", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        flowForm::label1032, _CANCEL_SLOW, "RemoveRateout", CHECKBOX, 0
    };
}


/*$SPECIALIZATION$*/


