/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionslinks.h"
#include "../main/includes/httraqt.h"

optionsLinks::optionsLinks(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::linksForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}

optionsLinks::~optionsLinks()
{
}

void optionsLinks::initTextPoints()
{
    *opts << (trWidgets) {
        linksForm::label1023, _ALL_LINKS_MATCH, "ParseAll", CHECKBOX, 0
    };

    *opts << (trWidgets) {
        linksForm::label1022, _GET_NHTML, "Near", CHECKBOX, 0
    };

    *opts << (trWidgets) {
        linksForm::label1021, _TEST_LINKS, "Test", CHECKBOX, 0
    };

    *opts << (trWidgets) {
        linksForm::label1024, _GET_HTML_FIRST, "HTMLFirst", CHECKBOX, 0
    };
}


/*$SPECIALIZATION$*/


