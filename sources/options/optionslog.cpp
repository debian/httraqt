/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionslog.h"
#include "../main/includes/httraqt.h"

optionsLog::optionsLog(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::logForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    QString t =  this->parent->translate(LISTDEF_9);
    QStringList sl = t.split("\n");
    logForm::comboLogType->addItems(sl);

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}


optionsLog::~optionsLog()
{
}


void optionsLog::initTextPoints()
{
    *opts << (trWidgets) {
        logForm::comboLogType, -1, "LogType", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        logForm::checkStoreAll, _STORE_CACHE, "StoreAllInCache", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        logForm::checkNoRecatch, _NO_REDOWNLOAD, "NoRecatch", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        logForm::checkLogFiles, _CREATE_LOG, "Log", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        logForm::checkMakeIndex, _MAKE_INDEX, "Index", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        logForm::checkBuildTopIndex, _MAKE_TOPINDEX, "BuildTopIndex", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        logForm::checkMakeWordbase, _MAKE_WORD_INDEX, "WordIndex", CHECKBOX, 0
    };
}


/*$SPECIALIZATION$*/


