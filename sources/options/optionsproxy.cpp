/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionsproxy.h"
#include "../main/includes/httraqt.h"

optionsProxy::optionsProxy(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::proxyForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    connect(proxyForm::labelHide, SIGNAL(toggled(bool)), this, SLOT(hidePass(bool)));

    initTextPoints();
}


optionsProxy::~optionsProxy()
{
}


void optionsProxy::hidePass(bool f)
{
    if (f == true) {
        proxyForm::editPass->setEchoMode(QLineEdit::Password);
    } else {
        proxyForm::editPass->setEchoMode(QLineEdit::Normal);
    }
}


void optionsProxy::initTextPoints()
{
    *opts << (trWidgets) {
        proxyForm::labelProxy, _PROX_ADDR, "", LABEL, 0
    };
    *opts << (trWidgets) {
        proxyForm::groupLogin, _PROX_DEFINE, "", GROUPBOX, 0
    };
    *opts << (trWidgets) {
        proxyForm::labelLogin, _LOGIN, "", LABEL, 0
    };
    *opts << (trWidgets) {
        proxyForm::labelPort, _PROX_PORT, "", LABEL, 0
    };
    *opts << (trWidgets) {
        proxyForm::labelPass, _PASS, "", LABEL, 0
    };
    *opts << (trWidgets) {
        proxyForm::labelHide, _HIDE_PASS, "", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        proxyForm::labelFtp, _USE_PROXY, "UseHTTPProxyForFTP", CHECKBOX, 0
    };
    *opts << (trWidgets) {
        proxyForm::editProxy, -1, "Proxy", EDITLINE,  ""
    };
    *opts << (trWidgets) {
        proxyForm::editPort, -1, "ProxyPort", EDITLINE,  ""
    };
    *opts << (trWidgets) {
        proxyForm::editLogin, -1, "ProxyLogin", EDITLINE,  ""
    };
    *opts << (trWidgets) {
        proxyForm::editPass, -1, "ProxyPass", EDITLINE,  ""
    };
}



/*$SPECIALIZATION$*/


