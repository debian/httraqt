/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/optionsrulez.h"
#include "includes/OptionsDialog.h"
#include "../main/includes/httraqt.h"


optionsRulez::optionsRulez(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::rulezForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    QString t = this->parent->translate(LISTDEF_1);
    QStringList sl = t.split("\n");
    rulezForm::comboBox2008->addItems(sl);

    ext[0] = "+*.odt +*.tex +*.pdf +*.doc +*.docx +*.rtf +*.txt +*.ps";
    ext[1] = "+*.gif +*.jpg +*.png +*.jpeg +*.ico +*.tif +*.bmp";
    ext[2] = "+*.zip +*.tar +*.tgz +*.gz +*.rar +*.z +*.exe +*.7z";
    ext[3] = "+*.mov +*.mpg +*.mpeg +*.avi +*.asf +*.mp3 +*.mp2 +*.rm +*.wav +*.vob +*.mkv +*.qt +*.swf +*.vid +*.ac3 +*.wma +*.wmv";

    chk[0] = rulezForm::label1023_2;
    chk[1] = rulezForm::label1021_3;
    chk[2] = rulezForm::label1022_2;
    chk[3] = rulezForm::label1023;

    for (int i = 0; i < 4; i++) {
        connect(chk[i], SIGNAL(clicked()), this, SLOT(onInc()));
    }

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();

    connect(rulezForm::label1094, SIGNAL(clicked()), this, SLOT(onIncludedLinks()));
    connect(rulezForm::label1093, SIGNAL(clicked()), this, SLOT(onExcludedLinks()));

    connect(rulezForm::scanList, SIGNAL(textChanged()), this, SLOT(setScanRulezToGUI()));
}


optionsRulez::~optionsRulez()
{
}


void optionsRulez::initTextPoints()
{
    *opts << (trWidgets) {
        rulezForm::labelHelp, _USE_WILDCARDS, "", LABEL,  0
    };
    *opts << (trWidgets) {
        rulezForm::textLabel1, _CRITERION, "", LABEL,  0
    };
    *opts << (trWidgets) {
        rulezForm::textLabel2, _STRING, "", LABEL,  0
    };
    *opts << (trWidgets) {
        rulezForm::labelExt, _TIP_GIF, "", LABEL,  0
    };
    *opts << (trWidgets) {
        rulezForm::scanList, -1, "WildCardFilters", TEXTEDIT,  ""
    };
    *opts << (trWidgets) {
        rulezForm::groupAddScan, _ADD_SCAN_RULE, "", GROUPBOX,  0
    };
    *opts << (trWidgets) {
        rulezForm::label1023_2, _DOCUMENTS, "", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        rulezForm::label1022_2, _ARCHIVES, "", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        rulezForm::label1021_3, _IMAGES, "", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        rulezForm::label1023, _MULTIMEDIA, "", CHECKBOX,  0
    };
}


void optionsRulez::setScanRulezToGUI()
{
    QString st;
    disconnect(rulezForm::scanList, SIGNAL(textChanged()), this, SLOT(setScanRulezToGUI()));
    st = rulezForm::scanList->toPlainText().replace("\n", " ");

    // set the widgets
    for (int i = 0; i < 4; i++) {
        detectTypeInc(st, i);
    }

    connect(rulezForm::scanList, SIGNAL(textChanged()), this, SLOT(setScanRulezToGUI()));
}


void optionsRulez::detectTypeInc(const QString &st, int i)
{
    //     QString editString = scanList->toPlainText();
    QStringList lst = ext[i].split(" ");

    for (QStringList::Iterator j = lst.begin(); j != lst.end(); j++) {
        if (st.indexOf((*j)) < 0) {
            return;
        }
    }

    disconnect(chk[i], SIGNAL(clicked()), this, SLOT(onInc()));
    chk[i]->setChecked(true);
    connect(chk[i], SIGNAL(clicked()), this, SLOT(onInc()));
}


void optionsRulez::onInc()
{
    QString editString = rulezForm::scanList->toPlainText();
    int pos = 0;

    for (; pos < 4; pos++) {
        if (sender() == chk[pos]) {
            break;
        }
    }

    QStringList lst = ext[pos].split(" ");

    if (chk[pos]->isChecked() == true) {
        for (QStringList::Iterator i = lst.begin(); i != lst.end(); i++) {
            if (editString.contains(*i) == false) {
                editString = (*i) + " " + editString;
            }
        }
    } else {
        for (QStringList::Iterator i = lst.begin(); i != lst.end(); i++) {
            if (editString.contains(*i) == true) {
                editString.remove(((*i) + " "));
                editString.remove(((*i) + "\n"));
            }
        }
    }

    rulezForm::scanList->setText(editString);
}

// true for include, false for exclude
void optionsRulez::onChangeLinks(bool incl)
{
    int action = rulezForm::comboBox2008->currentIndex();
    QString act;
    QString le = rulezForm::lineEdit356->text();

    if (le == "" && action != 10) {
        return;
    }

    disconnect(rulezForm::scanList, SIGNAL(textChanged()), this, SLOT(setScanRulezToGUI()));

    if (le.indexOf("*.") >= 0) {
        le.remove("*.");
    }

    switch (action) {

        case 0://File names with extension
            act = "*." + le;
            break;

        case 1://File names containing
            act = "*/*" + le + "*";
            break;

        case 2://This file name
            act = "*/" + le;
            break;

        case 3://Folder names containing
            act = "*/*" + le + "*/*";
            break;

        case 4://This folder name
            act = "*/" + le + "/*";
            break;

        case 5://Links on this domain
            act = "*[name]." + le + "/*";
            break;

        case 6://Links on domains containing
            act = "*[name].*[name]" + le + "[name].*[name]/*";
            break;

        case 7://Links from this host
            act = le + "/*";
            break;

        case 8://Links containing
            act = "*" + le + "*";
            break;

        case 9://This link
            act = le;
            break;

        case 10://ALL LINKS
            act = "*";
            break;
    }

    QString editor;

    editor = rulezForm::scanList->toPlainText();

    if (editor.contains("+" + act + " ") == true) {
        editor.remove("+" + act + " ");
        editor.remove("+" + act + "\n");
    }

    if (editor.contains("-" + act + " ") == true) {
        editor.remove("-" + act + " ");
        editor.remove("-" + act + "\n");
    }

    if (incl) {
        editor = "+" + act + " " + editor;
    } else {
        editor = "-" + act + " " + editor;
    }

    rulezForm::scanList->setText(editor);

    connect(rulezForm::scanList, SIGNAL(textChanged()), this, SLOT(setScanRulezToGUI()));

    rulezForm::lineEdit356->setText("");
}


void optionsRulez::onExcludedLinks()
{
    onChangeLinks(false);
}


void optionsRulez::onIncludedLinks()
{
    onChangeLinks(true);
}


/*$SPECIALIZATION$*/


