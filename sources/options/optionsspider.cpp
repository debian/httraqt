/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionsspider.h"
#include "../main/includes/httraqt.h"

optionsSpider::optionsSpider(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::spiderForm()
{
    setupUi(this);

    QString t;
    QStringList sl;

    this->parent = static_cast<OptionsDialog*>(parent);

    t = this->parent->translate(LISTDEF_7);
    sl = t.split("\n");
    spiderForm::label1238->addItems(sl);

    t = this->parent->translate(LISTDEF_8);
    sl = t.split("\n");
    spiderForm::label1033_2->addItems(sl);

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}

optionsSpider::~optionsSpider()
{
}


void optionsSpider::initTextPoints()
{
    *opts << (trWidgets) {
        spiderForm::label1245, _CHECK_DOC_TYPE, "", LABEL,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1256, _SPIDER, "", LABEL,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1237, _ACCEPT_COOK, "Cookies", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1239, _PARSE_JAVA, "ParseJava", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1242, _UPDATE_HACK, "UpdateHack", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1243, _URL_HACK, "URLHack", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1241, _TOL_REQ, "TolerantRequests", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1240, _FORCE_OLD, "HTTP10", CHECKBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1238, -1, "CheckType", COMBOBOX,  0
    };
    *opts << (trWidgets) {
        spiderForm::label1033_2, -1, "FollowRobotsTxt", COMBOBOX,  0
    };
}

/*$SPECIALIZATION$*/


