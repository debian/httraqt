#ifndef HTTVERSION_H
#define HTTVERSION_H

#define HTTQTVERSION  "1.4.11"
#define PROGRAM_DATE  "05 Jan 2023"

#define USE_QT_VERSION  6

#endif
